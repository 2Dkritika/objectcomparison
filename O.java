class O{
int id;
String name;

//parameterized constructor is created
O(int id,String name){
this.id=id;
this.name=name;
}

//override equals method
public boolean equals(Object obj){
if(obj!=null && obj instanceof O){}

//type casting
O E=(O)obj;
return ((id==E.id) && name.equals(E.name));
}
}

//Launcher method
class J{
public static void main(String[] args){
O X=new O(1,"abc");
O Y=null;

//Null Pointer Exception through try catch block
try{

System.out.println(X.equals(Y));
}catch(NullPointerException e)
{
System.out.println("null");
}
}
}